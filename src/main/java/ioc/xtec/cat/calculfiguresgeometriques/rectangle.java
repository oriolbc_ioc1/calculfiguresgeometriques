/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 *
 * @author oriol
 */
public class rectangle implements FiguraGeometrica{
    private final double base ;
    private final double altura;
    
    public rectangle(){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduïu la base del rectangle: ");
        this.base= scanner.nextDouble();  
        System.out.println("Introduïu l'altura del rectangle: ");
        this.altura=scanner.nextDouble();
        
    }

    @Override
    public double calcularArea() {
        return base*altura;
          
    }

    @Override
    public double calcularPerimetre() {
        return (base+altura)*2;
    }
    
    
}
